---
title: Crostini for Christmas - a gift from Google as Linux on Chromebook goes beta
date: "2018-12-16T22:12:03.284Z"
---
![delicous looking crostini photo by CHarles Haynes under https://creativecommons.org/licenses/by-sa/2.0/deed.en](./crostini.png)

**I**. **am**. ***VERY***. **excited**! So, as it's Christmas, I thought I'd share :)

If you're the sort of person that reads tech blogs, then it might not surprise you to know that right now I'm writing and preparing this post on a Linux machine. 

I'm using the wonderful Node.js, with its community-based modern Javascript ecosystem and which has simplified the life of a developer enormously in recent years. 

I'm developing in a bash shell on my local machine and I'm using tools like vs code and remarkable.

**But I'm doing it all on a Chromebook!** 

## A *nix dev environment on a Chromebook?
**YES!**

The announcement that Chromebooks would begin supporting running a Linux distro inside a virtual machine was actually [made back in May](https://blog.google/products/chromebooks/linux-on-chromebooks/) . 