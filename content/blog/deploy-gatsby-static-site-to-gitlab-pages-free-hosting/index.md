---
title: Host a blazing fast Gatsby static website for free with GitLab Pages
date: "2018-12-16T00:00:00.121Z"
---

### What you'll learn
* What you'll need to follow this post
* What are "static" websites?
* What is Gatsby js and why use it?
* What are GitLab (and GitHub) Pages
* How to use the Gatsby CLI to create and develop from a starter project
* How to deploy your Gatsby site to the GitLab Pages Continuous Integration (CI) pipeline.
* Deploiying other static sites to GitLab Pages
  
## tl;dr?

If that all sounds a bit tedious:
* choose if you want to follow this a Google cloudshell tutorial or on your local machine,
* check the blurb on variables and prequisites for credentials and environment below,
* jump to the **Quick Steps** section at the end for no-waffle instructions. 

If you don't mind the odd waffle or two, read on... 

## Options

### Two types of people: UsernameOrGroup and Project GitLab Sites



There are few different ways of doing this

### All you need is a browser! -  Working in Google Cloud Shell

gitlab api
https://docs.gitlab.com/ee/api/	

Basic usage
API requests should be prefixed with api and the API version. The API version is defined in lib/api.rb. For example, the root of the v4 API is at /api/v4.

Example of a valid API request using cURL:

curl "https://gitlab.example.com/api/v4/projects"

The API uses JSON to serialize data. You don’t need to specify .json at the end of an API URL.

Authentication
Most API requests require authentication, or will only return public data when authentication is not provided. For those cases where it is not required, this will be mentioned in the documentation for each individual endpoint. For example, the /projects/:id endpoint.

There are three ways to authenticate with the GitLab API:

OAuth2 tokens
Personal access tokens
Session cookie
For admins who want to authenticate with the API as a specific user, or who want to build applications or scripts that do so, two options are available:

Impersonation tokens
Sudo


### Variables
* To clarify when your version of the code will differ becauase of your personal setup, replaceable variables are written in the format {$YOUR-VARIABLE-NAME}: for example, {$YOUR-GITLAB-USERNAME}
* Rather than list them all here, they're in a seperate JSON file, with a bit of explanation, **here**.
* Some of these variables, for example {$YOUR-BLOG-TITLE} are sourced by Gatsby as site data.
* You can save the values of your variables or replace them in the text: just be aware that they may be publically accessible. Some variables which might contain  sensitive information are written as {$SENSITIVE!-YOUR-VARIABLE-NAME}, but that doesn't mean others may not be sensitive too.

### Accounts and credentials
* You need a [GitLab](https://gitlab.com)  account. You can sign in, or sign up at [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)  with a password or a 3rd party provider (currently GitHub, Google, Bitbucket or Twitter) if you don't have one.
* If you want to follow this as a cloudshell tutotial, you'll need a Google account. If you don't have one, or want to create one to use with this tutorial, [sign up for gmail here](https://mail.google.com/mail/signup).
* There's no need to enter a credit card or enable a billing agreement to follow the steps below. 
### Development environment

## QuickSteps

Bash your way through the steps with the minimum waffle. 


1. Sign in to Gitlab. 

		git remote set origin https://gitlab.com{$YOUR-GITLAB-USER_NAME}/{$YOUR-BLOG-GITLAB-REPO}



Create a NEW GROUP: https://gitlab.com/projects/new

PRESS NEW PROJECT GO TO https://gitlab.com/projects/new

The gourlaywheeler site at http://gourlaywheeler.gitlab.io - decriotion not markdown 

{uUSERNAME}.gitlab.io


Add a .gitignore


	curl https://github.com/gatsbyjs/gatsby/blob/master/.gitignore -o .gitignore

create a new empty branch like this:

	git checkout --orphan pages

stage files

	git add .

commit

	git commit -m "initial commit"


>warning: CRLF will be replaced by LF in node_modules/eol/.editorconfig.
>The file will have its original line endings in your working directory.


IT WILL FILL IN DEAFULATS FOR YOU

	git remote rename origin old-origin
	git remote add origin git@gitlab.com:gourlaywheeler/gourlaywheeler.gitlab.io.git
	git push -u origin pages
	//git push -u origin --tags
	
	
####	YAML FILE  .gitlab-ci.yml

	image: node:6.10.0
	
	pages:
	  script:
	  - npm install
	  - npm install gatsby-cli
	  - node_modules/.bin/gatsby build --prefix-paths
	  artifacts:
	    paths:
	    - public
	  cache:
	    paths:
	      - node_modules
	  only:
	  - pages
	

Fork (rename?), clone or download this project

Install Gatsby CLI
Generate and preview the website with hot-reloading: gatsby develop

Add content

Project name 
My awesome project
Project URL
https://gitlab.com/
Project URL Project URL Project URL Project URL 
Project slug 
my-awesome-project
Want to house several dependent projects under the same namespace? Create a group.
Project description (optional)

The Auto DevOps pipeline has been enabled and will be used if no alternative CI configuration file is found. More information Settings | Dismiss

URLs and Baseurls
Every Static Site Generator (SSG) default configuration expects to find your website under a (sub)domain (example.com), not in a subdirectory of that domain (example.com/subdir). Therefore, whenever you publish a project website (namespace.gitlab.io/project-name), you’ll have to look for this configuration (base URL) on your SSG’s documentation and set it up to reflect this pattern.

For example, for a Jekyll site, the baseurl is defined in the Jekyll configuration file, _config.yml. If your website URL is https://john.gitlab.io/blog/, you need to add this line to _config.yml:

baseurl: "/blog"

On the contrary, if you deploy your website after forking one of our default examples, the baseurl will already be configured this way, as all examples there are project websites. If you decide to make yours a user or group website, you’ll have to remove this configuration from your project. For the Jekyll example we’ve just mentioned, you’d have to change Jekyll’s _config.yml to:

baseurl: ""


Project Websites
You created a project called blog under your username john, therefore your project URL is https://gitlab.com/john/blog/. Once you enable GitLab Pages for this project, and build your site, it will be available under https://john.gitlab.io/blog/.
You created a group for all your websites called websites, and a project within this group is called blog. Your project URL is https://gitlab.com/websites/blog/. Once you enable GitLab Pages for this project, the site will live under https://websites.gitlab.io/blog/.
User and Group Websites
Under your username, john, you created a project called john.gitlab.io. Your project URL will be https://gitlab.com/john/john.gitlab.io. Once you enable GitLab Pages for your project, your website will be published under https://john.gitlab.io.
Under your group websites, you created a project called websites.gitlab.io. your project’s URL will be https://gitlab.com/websites/websites.gitlab.io. Once you enable GitLab Pages for your project, your website will be published under https://websites.gitlab.io.

https://docs.gitlab.com/ee/user/project/pages/introduction.html


How to set up GitLab Pages in a repository where there’s also actual code
Remember that GitLab Pages are by default branch/tag agnostic and their deployment relies solely on what you specify in .gitlab-ci.yml. You can limit the pages job with the only parameter, whenever a new commit is pushed to a branch that will be used specifically for your pages.

That way, you can have your project’s code in the master branch and use an orphan branch (let’s name it pages) that will host your static generator site.

You can create a new empty branch like this:

	git checkout --orphan pages

The first commit made on this new branch will have no parents and it will be the root of a new history totally disconnected from all the other branches and commits. Push the source files of your static generator in the pages branch.

Below is a copy of .gitlab-ci.yml where the most significant line is the last one, specifying to execute everything in the pages branch:

image: ruby:2.1

pages:
  script:
  - gem install jekyll
  - jekyll build -d public/
  artifacts:
    paths:
    - public
  only:
  - pages

See an example that has different files in the master branch and the source files for Jekyll are in a pages branch which also includes .gitlab-ci.yml.




USER PROJECT PAGES
https://docs.gitlab.com/ee/user/project/pages/


INTRO
https://docs.gitlab.com/ee/user/project/pages/introduction.html

FULL ON ADMIN GUIDE
https://docs.gitlab.com/ee/administration/pages/index.html

STATIC AND DYNAMIC SITES
https://about.gitlab.com/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/
